////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 4/24/18 CSE2 Spring 2018
//
// This program creates a virtual city using arrays that is then displayed,
// invaded, and then then updated as the invading robots move west. Occupied
// city blocks are represented by a negative number.
//
import java.util.Random;
public class RobotCity{
  public static int[][] buildCity(){
    Random random = new Random();
    int[][] city = new int[random.nextInt(6)+10][random.nextInt(6)+10];
    for(int i=0; i<city.length; i++){
      for(int j=0; j<city[i].length; j++){
        city[i][j] = random.nextInt(900) + 100;
      }
    }
    return city;
  }
  public static void display(int[][] city){
    for(int i=0; i<city.length; i++){
      for(int j=0; j<city[i].length; j++){
        System.out.printf("%4d", city[i][j]);
      }
      System.out.printf("\n");
    }
  }
  public static void invade(int[][] city, int k){
    Random random = new Random();
    while(k>0){
      int i = random.nextInt(city.length);
      int j = random.nextInt(city[0].length);
      if(city[i][j]>0){
        city[i][j] = -1*city[i][j];
        k--;
      }
    }
  }
  public static void update(int[][] city){
    for(int i=city.length-1; i>=0; i--){
      for(int j=city[i].length-1; j>=0; j--){
        if(city[i][j]<0){
          if(j==city[i].length-1 || city[i][j+1]<0){
            city[i][j]=-1*city[i][j];
          }
          else{
            city[i][j]=-city[i][j];
            city[i][j+1]=-1*city[i][j+1];
          }
        }
      }
    }
  }
  public static void main(String[] args){
    Random random = new Random();
    int[][] city = buildCity();
    display(city);
    int k = random.nextInt(60);
    System.out.println("Robots: "+k);
    invade(city,k);
    display(city);
    for(int i=1; i<5; i++){
      System.out.println(" ");
      update(city);
      display(city);
    }
  }
}