/////////////////////////////////////////////////////////
/// Dawson Wallace 2/3/18 CSE2 Spring 2018
//
//  This program computes and prints the number of minutes 
//for each trip, the number of counts for each trip, the
//distance of each trip in miles, and the distance for 
//the two trips combined for a bicycle using time elapsed
//in seconds and the number of wheel rotations recorded 
//by a cyclometer.
//
public class Cyclometer {
  // main method required for every java program
  public static void main(String[] args) {
      
     //input data
     int secsTrip1=480; //seconds elapsed in trip 1
     int secsTrip2=3220; //seconds elapsed in trip 2
     int countsTrip1=1561; //counts in trip 1
     int countsTrip2=9037; //counts in trip 2
    
      //intermediate variables and output data
      double wheelDiameter=27.0; //the diameter of the wheel
      double PI=3.14159; //PI is used later in the circumfrance formula
      double feetPerMile=5280; //used to convert between feet and miles
      double inchesPerFoot=12; //used to convert between inches and feet
      double secondsPerMinute=60; //used to convert between seconds and minutes
      double distanceTrip1; //distance of trip 1
      double distanceTrip2; //distance of trip 2
      double totalDistance; //total distance of both trips
        
        //prints numbers that have been stored
        System.out.println("Trip 1 took "+
          (secsTrip1/secondsPerMinute)+" minutes and had "+
                                 countsTrip1+" counts. ");
        System.out.println("Trip 2 took "+
           (secsTrip2/secondsPerMinute)+" minutes and had "+
                                 countsTrip2+" counts. ");
    
    //calculations
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives distance in inches 
    //(for each count, a rotation of the wheel tavels
    //the diameter in inches times PI)
    distanceTrip1=distanceTrip1/inchesPerFoot/feetPerMile; //Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; 
    totalDistance=distanceTrip1+distanceTrip2; //gives total distance
    
      //Prints out the output data
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
      System.out.println("The total distance was "+totalDistance+" miles");
    } //end of main method 
}