/////////////////////////////////////////////////////////
/// Dawson Wallace 2/9/18 CSE2 Spring 2018
//
//Finds the amount each person pays when a check is split
//evenly bases on the original cost of the check, the 
//the percentage tip they wish to pay, and the number of 
//ways the check is split. 
//
import java.util.Scanner;
public class Check{
    //main method required for every java program
    public static void main(String[] args) {
      //declares sacanner
      Scanner myScanner = new Scanner( System.in );
        
       //prompts the user for original cost of check
       System.out.print("Enter the original cost of the check in the form xx.xx: ") ;
       //accepts the users input
       double checkCost = myScanner.nextDouble();
       
       //prompts the user for the tip percentage they wish to pay
       System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ") ;
       //accepts the users input
       double tipPercent = myScanner.nextDouble();
       //converts percent into a decimal
       tipPercent /=100;
       
       //prompts the user for the number of people the check is split between and accepts their input
       System.out.print("Enter the number of people who went out to dinner: ") ;
       int numPeople = myScanner.nextInt();
       
    //prints out the amount each person needs to pay
    double totalCost;
    double costPerPerson;
    int dollars; //used for storing the
    int dimes;   //individual digits
    int pennies; //of the cost
    totalCost = checkCost*(1+tipPercent);
    costPerPerson = totalCost/numPeople;
    dollars=(int) costPerPerson; //drops decimals
    //% returns the remainder after division
    dimes=(int) (costPerPerson*10) % 10; //gets dimes amount
    pennies=(int) (costPerPerson*100) %10; //gets pennies amount
    //prints final value
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies) ;
       
      
    } //end of main method
}//end of class