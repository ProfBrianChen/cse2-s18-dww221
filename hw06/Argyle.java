////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/20/18 CSE2 Spring 2018
//
// Displays an argyle patern in text
//
import java.util.Scanner;
public class Argyle{
  public static void main(String[] args){
    //declares scanner
    Scanner myScanner = new Scanner(System.in);
    //declares variables 
    int widthVW=0, heightVW=0, widthAD=0, widthAS=0;
    char char1='A', char2='A', char3='A';
    //prompts user for all inputs and accepts inputs if acceptable and returns error if not
    System.out.print("Please enter a positive integer for the width of Viewing window in characters: ");
    while(myScanner.hasNextInt()){
      widthVW=myScanner.nextInt();
      if(widthVW>0){
        break;
      }
      else{
        System.out.print("Make sure you enter a positive intergral: ");
      }
    }
    System.out.print("Please enter a positive integer for the height of Viewing window in characters: ");
    while(myScanner.hasNextInt()){
      heightVW=myScanner.nextInt();
      if(heightVW>0){
        break;
      }
      else{
        System.out.print("Make sure you enter a positive intergral: ");
      }
    }
    System.out.print("Please enter a positive integer for the width of the argyle diamonds: ");
     while(myScanner.hasNextInt()){
      widthAD=myScanner.nextInt();
      if(widthAD>0){
        widthAD*=2;
        break;
      }
      else{
        System.out.print("Make sure you enter a positive intergral: ");
      }
    }
    System.out.print("Please enter a positive odd integer for the width of the argyle center strip: ");
    while(myScanner.hasNextInt()){
      widthAS=myScanner.nextInt();
      if(widthAS>0 && widthAS%2==1 && widthAS <= widthAD/2){
        break;
      }
      else{
        System.out.print("Make sure the integer is not larger than half of the diamond size: ");
      }
    }
    System.out.print("Please enter a first character for the pattern fill: ");
    String temp1=myScanner.next();
    char1=temp1.charAt(0);
    System.out.print("Please enter a second character for the pattern fill: ");
    String temp2=myScanner.next();
    char2=temp2.charAt(0);
    System.out.print("Please enter a third character for the stripe fill: ");
    String temp3=myScanner.next();
    char3=temp3.charAt(0);
    //declares variable
    int widthVWOriginal=widthVW;
    //prints pattern
    for(int i=0; i<heightVW; i++){
      for(int j=0; j<widthVW; j++){
        //strip
        if(i>=j-widthAS/2 && i<=j+widthAS/2 || widthAD-i>=j+1-widthAS/2 && widthAD-i<=j+1+widthAS/2){
          System.out.print(char3);
        }
        //diamond
        else if(i+j>=widthAD/2 && j<widthAD/2 && i<widthAD/2){
          System.out.print(char2);
        }
        else if(i>j-widthAD/2 && j>=widthAD/2 && i<widthAD/2){
          System.out.print(char2);
        }
        else if(j>=i-widthAD/2 && j<widthAD/2 && i>=widthAD/2){
          System.out.print(char2);
        }
        else if(i+j-widthAD<widthAD/2 && j>=widthAD/2 && i>=widthAD/2){
          System.out.print(char2);
        }
        //fill
        else{
          System.out.print(char1);
        }
        //pattern repetition horizontal
        if(j+1==widthAD){
          j=0;
          widthVW-=widthAD;
        }
      }
      System.out.println(" ");
      //pattern repetition vertical
      widthVW=widthVWOriginal;
      if((i+1)==widthAD){
        i=0;
        heightVW-=widthAD;
      }
    }  
  }
}