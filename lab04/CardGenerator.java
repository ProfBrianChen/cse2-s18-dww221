//////////////////////////////////////////////////////////////
/// Dawson Wallace 2/16/18 CSE2 Spring 2018
//
// This program picks a random card from a normal deck 
// of 52 cards
//
public class CardGenerator{
  //main method required for every java program
  public static void main(String[] args){
    //generates random number between 1 and 52
    int num = (int)(Math.random()*51)+1;
    //chooses the suit based on the number generated and prints
    String suit;
    if (num<14){
      suit = "Diamonds";
    }
    else if (num<27){
      suit = "Clubs";
    }
    else if (num<40){
      suit = "Hearts";
    }
    else{
      suit = "Spades";
    }
    //switches certain number values to face cards and prints card
    switch (num){
      case 1: case 14: case 27: case 40:
        System.out.println("You picked the Ace of "+suit);
        break;
      case 11: case 24: case 37: case 50:
        System.out.println("You picked the Jack of "+suit);
        break;
      case 12: case 25: case 38: case 51:
        System.out.println("You picked the Queen of "+suit);
        break;
      case 13: case 26: case 39: case 52:
        System.out.println("You picked the King of "+suit);
        break;
      case 2: case 15: case 28: case 41:
        System.out.println("You picked the 2 of "+suit);
        break;
      case 3: case 16: case 29: case 42:
        System.out.println("You picked the 3 of "+suit);
        break;
      case 4: case 17: case 30: case 43:
        System.out.println("You picked the 4 of "+suit);
        break;
      case 5: case 18: case 31: case 44:
        System.out.println("You picked the 5 of "+suit);
        break;
      case 6: case 19: case 32: case 45:
        System.out.println("You picked the 6 of "+suit);
        break;
      case 7: case 20: case 33: case 46:
        System.out.println("You picked the 7 of "+suit);
        break;
      case 8: case 21: case 34: case 47:
        System.out.println("You picked the 8 of "+suit);
        break;
      case 9: case 22: case 35: case 48:
        System.out.println("You picked the 9 of "+suit);
        break;
      case 10: case 23: case 36: case 49:
        System.out.println("You picked the 10 of "+suit);
        break;
    }
  }
}