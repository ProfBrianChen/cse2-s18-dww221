/////////////////////////////////////////////////
/// Dawson Wallace 2/2/18 CSE2 Spring 2018
//
// Computes cost of items plus tax
public class Arithmetic {
  public static void main(String[] args) {
    //Input variables
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99; //Cost per shirt 
    
    int numBelts = 1; //Number of Belts
    double beltCost = 33.99; //Cost per belt
    
    double paSalesTax = 0.06; //The tax rate
    
      //Output Variables calculation and elimination of fractional pennies
      double totalCostOfPants = numPants * pantsPrice; //Total cost of pants
      double salesTaxPants = ( (double) ( (int) ((totalCostOfPants * paSalesTax) * 100))) / 100; //Sales tax charged buying pants

      double totalCostOfShirts = numShirts * shirtPrice; //Total cost of sweatshirts
      double salesTaxShirts = ( (double) ( (int) ((totalCostOfShirts * paSalesTax) * 100))) / 100; //Sales tax charged buying sweatshirts

      double totalCostOfBelts = numBelts * beltCost; //Total cost of belts
      double salesTaxBelts = ( (double) ( (int) ((totalCostOfBelts * paSalesTax) * 100))) / 100; //Sales tax charged buying belts
    
      double costOfPurchase = totalCostOfBelts + totalCostOfShirts + totalCostOfPants; //Total cost before sales tax
      double totalSalesTax = ( (double) ( (int) ((costOfPurchase * paSalesTax) * 100))) / 100; //Total sales tax
    
      double totalPaid = costOfPurchase + totalSalesTax; //Total paid for this transacion
        
         //Prints output variables
         System.out.println("Total Cost of Pants:"); System.out.println(totalCostOfPants);
         System.out.println("Pants Sales Tax:"); System.out.println(salesTaxPants);
            System.out.println(" "); //seperates different iteams in display
         System.out.println("Total Cost of Shirts:"); System.out.println(totalCostOfShirts);
         System.out.println("Shirts Sales Tax:"); System.out.println(salesTaxShirts);
            System.out.println(" "); //seperates different iteams in display
         System.out.println("Total Cost of Belt:"); System.out.println(totalCostOfBelts);
         System.out.println("Belts Sales Tax:"); System.out.println(salesTaxBelts);
            System.out.println(" "); //seperates different iteams in display
         System.out.println("Total Cost Before Sales Tax:"); System.out.println(costOfPurchase);
         System.out.println("Total Sales Tax:"); System.out.println(totalSalesTax);
            System.out.println(" "); //seperates different iteams in display
         System.out.println("Total Paid:"); System.out.println(totalPaid);
         
  }
}