////////////////////////////////////////////////////////////////////
/// Dawson Wallace 2/12/2018 CSE2 Spring 2018
//
// Asks the user for number of acres of land affected by hurricane
// precipitation and how many inches of rain were dropped on average
// and converts the inputs into the quantity of rain in cubic miles
//
import java.util.Scanner;
public class Convert {
  //main method required for every java program
  public static void main(String[] args) {
    //declares scanner
    Scanner myScanner = new Scanner( System.in );
    
        //prompts user to input number of acres affected
        System.out.print("Enter the affected area in acres: ");
        //accepts the users input
        double acres = myScanner.nextDouble();
        //prompts user to input how many inches of rain were dropped on average 
        System.out.print("Enter the rainfall in the affected area: ");
        //accepts the users input
        double inches = myScanner.nextDouble();
    
          //Useful constants
          double acresPerSqMile = 640.0; //acres per square mile
          double inchesPerMile = 63360.0; //inches per mile
            
     //Calculations
     double rain = (acres/acresPerSqMile) * (inches/inchesPerMile); //base * height = area
     
       //prints the quantity of rain in cubic miles
       System.out.println(rain + " cubic miles");
    
  }//end of main method
}//end of class