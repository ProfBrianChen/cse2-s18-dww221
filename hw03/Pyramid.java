//////////////////////////////////////////////////////////
/// Dawson Wallace 2/12/18 CSE2 Spring 2018
//
// Prompts the user for the dimensions of a pyramid and
// returns the volume inside the pyramid
//
import java.util.Scanner;
public class Pyramid {
    //main method required for every java program
    public static void main(String[] args) {
      
      //declares scanner
      Scanner myScanner = new Scanner( System.in );
      
        //prompts for length input
        System.out.print("The square side of the pyramid is (input length): ");
        //accepts user input
        double length = myScanner.nextDouble();
        //prompts for height input
        System.out.print("The height of the pyramid is (input height): ");
        //accepts user input
        double height = myScanner.nextDouble();
      
      //calculating volume
      double volume = (Math.pow(length, 2.0) * height) / 3;
        
        //prints pyramid volume
        System.out.println("The volume inside the pyramid is: " + volume);
      
    }//end of main method 
}//end of class