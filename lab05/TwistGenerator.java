/////////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/8/18 CSE2 Spring 2018
// 
// Writes and prints a twist based on a length given by
// the user.
//
import java.util.Scanner;
public class TwistGenerator{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int length=0;//initializes length
    //prompts the user to input the length
    while(1==1){
      System.out.print("Input the length integer: ");
      if(myScanner.hasNextInt()){
        length = myScanner.nextInt();//makes sure the input is a possitve integer
        break;
      }
      else{
        String junkWord = myScanner.next();
        System.out.println("Makes sure it's a positive integer");
      }
    }
    //prints twist
    int lengthOriginal = length;//used to reset length after each line printed 
    for (int i=2; i < length; length-=3){
      System.out.print("\\ /");     
    }
    //finishes off twist line 1
    if(length==0){
      System.out.println(" ");
    }
    if(length==2){
      System.out.println("\\ ");
    }
    if(length==1){
      System.out.println("\\");
    }//
    //resests for line 2
    length = lengthOriginal;
    for (int i=2; i < length; length-=3){
      System.out.print(" x ");
    }
    //finishes off twist line 2
    if(length==0){
      System.out.println(" ");
    }
    if(length==2){
      System.out.println(" x");
    }
    if(length==1){
      System.out.println(" ");
    }//
    //resets for line 3
    length = lengthOriginal;
    for (int i=2; i < length; length-=3){
      System.out.print("/ \\");
    }
    //finishes off twist 
    if(length==0){
      System.out.println(" ");
    }
    if(length==2){
      System.out.println("/ ");
    }
    if(length==1){
      System.out.println("/");
    }//
  }
}