//////////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/6/18 CSE2 spring 2018
//
// Asks the user to enter information relating to a course they
// are currently taking. Asks for course number, department name, 
// the number of times it meets in a week, the time the class starts,
// the instructor name, and the number of students.
//
import java.util.Scanner;
public class Hw05{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    //asks for course number
    int courseNum = 0;
    while(1==1){
      System.out.print("Course number: ");
      if(myScanner.hasNextInt()){
        courseNum = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Please input an integer");
        myScanner.next();
      }
    } 
    
    //asks for department name
    String depName = "asdf";
    while(1==1){
      System.out.print("Department name: ");
      if(myScanner.hasNextInt()){
        System.out.println("Please input a word");
        myScanner.next();
      }
      else if(myScanner.hasNextDouble()){
        System.out.println("Please input a word");
        myScanner.next();
      }
      else{
        depName = myScanner.next();
        break;
      }
    }
    
    //asks for number of times it meets in a week
    int weekNum = 0;
    while(1==1){
      System.out.print("Number of times the class meets in a week: ");
      if(myScanner.hasNextInt()){
        weekNum = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Please input an integer");
        myScanner.next();
      }
    } 
    
    //asks for the time the class starts
    int timeHour = 0; int timeMinute = 0; String timeAMPM = "asdf";
    while(1==1){
      System.out.print("Time class starts ('Hour' space 'Minute'): ");
      if(myScanner.hasNextInt()){
        timeHour = myScanner.nextInt();//hour
        if(myScanner.hasNextInt()){
          timeMinute = myScanner.nextInt();//minute
          break;
        }
        else{
          System.out.println("Please input an integer");
          myScanner.next();
        }
        break;
      }
      else{
        System.out.println("Please input an integer");
        myScanner.next();
      }
    } 
    
    //asks for the instructors name
    String insName = "asdf";
    while(1==1){
      System.out.print("Instructor name: ");
      if(myScanner.hasNextInt()){
        System.out.println("Please input a word");
        myScanner.next();
      }
      else if(myScanner.hasNextDouble()){
        System.out.println("Please input a word");
        myScanner.next();
      }
      else{
        insName = myScanner.next();
        break;
      }
    }
    
    //asks for the number of students
    int students = 0;
    while(1==1){
      System.out.print("Number of students: ");
      if(myScanner.hasNextInt()){
        students = myScanner.nextInt();
        break;
      }
      else{
        System.out.println("Please input an integer");
        myScanner.next();
      }
    } 
    
    
    //prints all class information
    System.out.println("----------------------------------------------------");
    System.out.println("Course number: "+courseNum);
    System.out.println("Department name: "+depName);
    System.out.println("Number of times the class meets per week: "+weekNum);
    System.out.println("Time class Starts: "+ timeHour +":"+ timeMinute);
    System.out.println("Instructor name: "+insName);
    System.out.println("Number of students: "+students);
    System.out.println("----------------------------------------------------");
  }
}