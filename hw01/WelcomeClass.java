///////////////////////////////////////////////////
/// Dawson Wallace 2/2/18 CSE2 Spring 2018
//
public class WelcomeClass {
  
    public static void main(String[] args) {
      //        -----------
      // Prints | WELCOME | to terminal window
      //        -----------
      System.out.println("-----------");
      System.out.println("| WELCOME |");
      System.out.println("-----------");
        //          ^  ^  ^  ^  ^  ^
        //         / \/ \/ \/ \/ \/ \
        // Prints <-D--W--W--2--2--1-> to terminal window
        //         \ /\ /\ /\ /\ /\ /
        //          v  v  v  v  v  v 
        System.out.println("  ^  ^  ^  ^  ^  ^  ");
        System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
        System.out.println("<-D--W--W--2--2--1->");
        System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
        System.out.println("  v  v  v  v  v  v  ");
          // Prints autobiographic statement to terminal window 
          System.out.print("I'm Dawson, I'm 18, and this is my first year at Lehigh. I'm a math major, a marching band member, and 1 in a family of 9.");
    }
}