///////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/26/18 CSE2 Sprng 2018
//
// Generates random stories with correct grammar
//
import java.util.Random; //imports random input statement 
import java.util.Scanner; 
public class Story{
  //method used to generate random adjective
  public static String adjective(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String word = "asdf";
    switch(randomInt){
      case 0: word="quick"; break;
      case 1: word="slow"; break;
      case 2: word="tall"; break;
      case 3: word="short"; break;
      case 4: word="red"; break;
      case 5: word="ivisible"; break;
      case 6: word="green"; break;
      case 7: word="zealous"; break;
      case 8: word="tenacious"; break;
      case 9: word="tremulous"; break;
    }
    return word;
  }
  //generates random subject
  public static String subject(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String word = "asdf";
    switch(randomInt){
      case 0: word="pterodactyl"; break;
      case 1: word="corgi"; break;
      case 2: word="shark"; break;
      case 3: word="bear"; break;
      case 4: word="fish"; break;
      case 5: word="tree"; break;
      case 6: word="soldier"; break;
      case 7: word="dolphin"; break;
      case 8: word="velociraptor"; break;
      case 9: word="scienist"; break;
    }
    return word;
  }
  //generates random past tense verb
  public static String verb(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String word = "asdf";
    switch(randomInt){
      case 0: word="jumped"; break;
      case 1: word="watched"; break;
      case 2: word="discussed"; break;
      case 3: word="tripped"; break;
      case 4: word="asked"; break;
      case 5: word="freed"; break;
      case 6: word="trapped"; break;
      case 7: word="helped"; break;
      case 8: word="raced"; break;
      case 9: word="fired"; break;
    }
    return word;
  }
  //generates random object
  public static String object(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String word = "asdf";
    switch(randomInt){
      case 0: word="dog"; break;
      case 1: word="cat"; break;
      case 2: word="goat"; break;
      case 3: word="clam"; break;
      case 4: word="volleyball"; break;
      case 5: word="bird"; break;
      case 6: word="fox"; break;
      case 7: word="robot"; break;
      case 8: word="alien"; break;
      case 9: word="plant"; break;
    }
    return word;
  }
  public static String thesis(){
     Scanner myScanner = new Scanner(System.in);
    while(1==1){
      String subject = Story.subject();
      System.out.println("The "+Story.adjective()+" "+Story.adjective()+" "+subject+" "+Story.verb()+" the "+Story.adjective()+" "+Story.object()+".");
      System.out.println("Would you like another sentence (enter '1')?");
      if(myScanner.nextInt()==1){
        continue;
      }
      else{
        return subject;
      }
    }
  }
  public static void main(String[] args){
    System.out.println("This "+Story.thesis()+" was "+Story.adjective()+" to the "+Story.adjective()+" "+Story.object()+".");
    System.out.println("It used the "+Story.object()+" and "+Story.verb()+" the "+Story.adjective()+" "+Story.subject()+".");
  }
}