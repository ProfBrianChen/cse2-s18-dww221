Grade:   99/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
Code compiles correctly.

B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
Code runs without runtime errors but occasionally prints out the suit of the card as 'asdf'.

C) How can any runtime errors be resolved?
N/A 

D) What topics should the student study in order to avoid the errors they made in this homework?
In lines 32 & 33 you name the face and suit 'asdf' (I think this was just a mistake that you forgot to come back to but try to read through your code to check for errors in the future before turning it in)

E) Other comments:
Overall, very nice job on this assignment. 