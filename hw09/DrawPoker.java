/////////////////////////////////////////////////////////////
/// Dawson Wallace 4/17/18 CSE2 Spring 2018
//
// Shuffles a deck draws two hands of five, then declares
// a winner based on the rules of draw poker.
//
import java.util.Random;
public class DrawPoker{
  // shuffles deck
  public static void shuffle(int[] deck){
    Random random = new Random();
    for(int i = 0; i<52; i++){
      int n = i + random.nextInt(52-i);
      int temp = deck[i];
      deck[i] = deck[n];
      deck[n] = temp;
    }
  }
  // prints hands in from smallest to largest 
  public static void print(int[] hand){
    // reorganizes in natural order
    for(int i=0; i<5; i++){
      for(int j=i+1; j<5; j++){
        if(hand[i]%13>hand[j]%13 || hand[i]%13==0){
          int temp = hand[i];
          hand[i]=hand[j];
          hand[j]=temp;
        }
      }
    }
    for(int i=0; i<5; i++){
      String suit = "asdf";
      String face = "asdf";
      switch(hand[i]%13){
        case 0: face="Ace"; break;
        case 10: face="Jack"; break;
        case 11: face="Queen"; break;
        case 12: face="king"; break;   
      }
      switch((int) (hand[i]/13)){
        case 0: suit="diamonds"; break;
        case 1: suit="clubs"; break;
        case 2: suit="hearts"; break;
        case 3: suit="spades"; break;
      }
      if(hand[i]%13>0 && hand[i]%13<10){
        System.out.println(hand[i]%13+1+" of "+suit+" ");
      }
      else{
        System.out.println(face+" of "+suit+" ");
      }
    }
    System.out.println(" ");
  }
  // looks for pairs 
  public static boolean pair(int[] hand){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        if(j==i){
          continue;
        }
        else if(hand[i]%13==hand[j]%13){
          return true;
        }
      }
    }
    return false;
  }
  // looks for three of a kind
  public static boolean threeOfAKind(int[] hand){
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        if(j==i){
          continue;
        }
        else if(hand[i]%13==hand[j]%13){
          for(int k=0; k<5; k++){
            if(k==i || k==j){
              continue;
            }
            else if(hand[k]%13==hand[i]%13){
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  // looks for flush
  public static boolean flush(int[] hand){
    if(hand[0]/13==hand[1]/13 && hand[1]/13==hand[2]/13 && hand[2]/13==hand[3]/13 && hand[3]/13==hand[4]/13){
      return true;
    }
    else{
      return false;
    }
  }
  // looks for full house
  public static boolean fullHouse(int[] hand){
    boolean triple = false;
    int val = 100;
    boolean pair = false;
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        if(j==i){
          continue;
        }
        else if(hand[i]%13==hand[j]%13){
          for(int k=0; k<5; k++){
            if(k==i || k==j){
              continue;
            }
            else if(hand[k]%13==hand[i]%13){
              triple = true;
              val = hand[k]%13;
            }
          }
        }
      }
    }
    for(int i=0; i<5; i++){
      for(int j=0; j<5; j++){
        if(j==i || hand[i]%13==val){
          continue;
        }
        else if(hand[i]%13==hand[j]%13){
          pair = true;
        }
      }
    }
    if(pair && triple){
      return true;
    }
    else{
      return false;
    }
  }
  public static void main(String[] args){
    int[] deck = new int[52];
    for(int i=0; i<52; i++){
      deck[i] = i+1;
    }
    shuffle(deck);
    //deals two hands
    int[] hand1 = new int[5];
    int[] hand2 = new int[5];
    for(int i=0; i<10; i++){
      if(i%2==0){
        hand1[i/2]=deck[i];
      }
      else{
        hand2[(i-1)/2]=deck[i];
      }
    }
    // prints hands
    System.out.println("Hand 1: ");
    print(hand1);
    System.out.println("Hand 2: ");
    print(hand2);
    // desides winner
    if((pair(hand1) && pair(hand2)) || (threeOfAKind(hand1) && threeOfAKind(hand2)) || (flush(hand1) && flush(hand2)) || (fullHouse(hand1) && fullHouse(hand2)) || hand1[4]==hand2[4]){
      System.out.println("Tie");
    }
    else if(fullHouse(hand1)){
      System.out.println("Hand 1 wins");
    }
    else if(fullHouse(hand2)){
      System.out.println("Hand 2 wins");
    }
    else if(flush(hand1)){
      System.out.println("Hand 1 wins");
    }
    else if(flush(hand2)){
      System.out.println("Hand 2 wins");
    }
    else if(threeOfAKind(hand1)){
      System.out.println("Hand 1 wins");
    }
    else if(threeOfAKind(hand2)){
      System.out.println("Hand 2 wins");
    }
    else if(pair(hand1)){
      System.out.println("Hand 1 wins");
    }
    else if(pair(hand2)){
      System.out.println("Hand 2 wins");
    }
    
    else if(hand1[4]>hand2[4]){
      System.out.println("Hand 1 wins");
    }
    else if(hand2[4]>hand1[4]){
      System.out.println("Hand 2 wins");
    }
    
  }
}