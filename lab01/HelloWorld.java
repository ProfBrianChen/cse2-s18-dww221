//////////////////////////////////////////////////////////////////////
///CSE 2 Hello World
///
public class HelloWorld {
  public static void main(String[] args) {
    int low = 1;
    int high = 3;
    int total = 0;
    while(low<=high){
      total += low;
      low++;
    }
    System.out.println(total);
  }  
}