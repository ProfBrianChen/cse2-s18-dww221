//////////////////////////////////////////////////
/// Dawson Wallace 2/20/18 CSE2 Spring 2018
//
// Perfoms a random roll of the dice and scores 
// the roll in Yahtzee. Prints out upper 
// section initial total, upper section total 
// including bonus, lower section total, and grand total.
//
import java.util.Scanner;
public class Yahtzee{
  //main method required for every java program
  public static void main(String[] args){
    //declares scanner
    Scanner myScanner = new Scanner( System.in );
   
   //lets user choose roll or randomize roll
    System.out.print("Type 1 for a specific roll or 2 for a random roll: ");
    int rollType = myScanner.nextInt();
    int roll=0; // declares roll variable for future use
    int dice1=0;
    int dice2=0;
    int dice3=0;
    int dice4=0;
    int dice5=0;
    if (rollType==1){ //lets user input specific roll
      System.out.print("Input 5 digit number to represent a specif roll: ");
      roll = myScanner.nextInt();
      //returns error for incorrect input
      if (roll>66666 || roll<11111 || roll%10>6 || roll%100>66 || roll%1000>666 || roll%10000>6666 || roll%10==0 || (roll/10)%100==0 || (roll%1000)==0 || (roll%10000)==0){
        System.out.println("Roll not valid");
        System.exit(0);
      }
      //seperates digits into individual dice
      dice1=roll%10;
      dice2=((int)(roll/10))%10;
      dice3=((int)(roll/100))%10;
      dice4=((int)(roll/1000))%10;
      dice5=(int)(roll/10000);
    }
    else if (rollType==2){//creates random roll
      dice1=(int)((Math.random()*5)+1);
      dice2=(int)((Math.random()*5)+1);
      dice3=(int)((Math.random()*5)+1);
      dice4=(int)((Math.random()*5)+1);
      dice5=(int)((Math.random()*5)+1);
      roll=dice5*10000+dice4*1000+dice3*100+dice2*10+dice1;//gives 5 digit roll
    }
    else{//rejects invald roll type
      System.out.println("Invalid input");
      System.exit(0);
    }
    System.out.println("Roll: "+roll);//so user can see random roll
    
    //finds how many of each number
    int one=0; int two=0; int three=0; int four=0; int five=0; int six=0;
    switch (dice1){
      case 1: one=one+1; break;
      case 2: two=two+1; break;
      case 3: three=three+1; break;
      case 4: four=four+1; break;
      case 5: five=five+1; break;
      case 6: six=six+1; break;
    }
     switch (dice2){
      case 1: one=one+1; break;
      case 2: two=two+1; break;
      case 3: three=three+1; break;
      case 4: four=four+1; break;
      case 5: five=five+1; break;
      case 6: six=six+1; break;
    }
     switch (dice3){
      case 1: one=one+1; break;
      case 2: two=two+1; break;
      case 3: three=three+1; break;
      case 4: four=four+1; break;
      case 5: five=five+1; break;
      case 6: six=six+1; break;
    }
     switch (dice4){
      case 1: one=one+1; break;
      case 2: two=two+1; break;
      case 3: three=three+1; break;
      case 4: four=four+1; break;
      case 5: five=five+1; break;
      case 6: six=six+1; break;
    }
     switch (dice5){
      case 1: one=one+1; break;
      case 2: two=two+1; break;
      case 3: three=three+1; break;
      case 4: four=four+1; break;
      case 5: five=five+1; break;
      case 6: six=six+1; break;
    }
    
   //upper section
    int upper = dice1 + dice2 + dice3 + dice4 + dice5;
    System.out.println("Upper section initial total: "+upper);
 
   //upper section plus bonus
    int upperBonus=0;//declares upper plus bonus for further use
    if (upper>63){
      upperBonus=upper+35;
    }
    else{
      upperBonus=upper;
    }
    System.out.println("Upper section total including bonus: "+upperBonus);
    
   //Lower section
    int lower=0;
    //three of a kind
    if (one==3){
      lower=lower+3;
    }
    if (two==3){
      lower=lower+6;
    }
    if (three==3){
      lower=lower+9;
    }
    if (four==3){
      lower=lower+12;
    }
    if (five==3){
      lower=lower+15;
    }
    if (six==3){
      lower=lower+18;
    }
    //four of a kind
    if (one==4){
      lower=lower+4;
    }
    if (two==4){
      lower=lower+8;
    }
    if (three==4){
      lower=lower+12;
    }
    if (four==4){
      lower=lower+16;
    }
    if (five==4){
      lower=lower+20;
    }
    if (six==4){
      lower=lower+24;
    }
    //full house
    boolean oneFull = one==2 && (two==3 || three==3 || four==3 || five==3 || six==3);
    boolean twoFull = two==2 && (one==3 || three==3 || four==3 || five==3 || six==3);
    boolean threeFull = three==2 && (two==3 || one==3 || four==3 || five==3 || six==3);
    boolean fourFull = four==2 && (two==3 || three==3 || one==3 || five==3 || six==3);
    boolean fiveFull = five==2 && (two==3 || three==3 || four==3 || one==3 || six==3); 
    boolean sixFull = six==2 && (two==3 || three==3 || four==3 || five==3 || one==3);
    if (oneFull || twoFull || threeFull || fourFull || fiveFull || sixFull){
      lower=lower+25;
    }
    //small straight
    if ((one==1 && two==1 && three==1 && four==1) || (two==1 && three==1 && four==1 && five==1) || (three==1 && four==1 && five==1 && six==1)){
      lower=lower+30;
    }
    //large straight
    if ((one==1 && two==1 && three==1 && four==1 && five==1) || (two==1 && three==1 && four==1 && five==1 && six==1)){
      lower=lower+40;
    }
    //Yahtzee
    if (one==5 || two==5 || three==5 || four==5 || five==5 || six==5){
      lower = lower+50;
    }
    //chance
    lower = lower + dice1 + dice2 + dice3 + dice4 + dice5;
    //prints lower total
    System.out.println("Lower section total: "+lower);
    
    //Grand total
    int grandTotal=upperBonus+lower;
    System.out.println("Grand total: "+grandTotal);
        
  }//end of main method
}//end of class