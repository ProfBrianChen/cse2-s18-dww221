/////////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 4/6/18 CSE2 Spring 2018
//
// Uses two arrays with user inputs to display student midterm grades
//
import java.util.Scanner;
import java.lang.Math;
public class StudentGrades{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    // student names
    int numStudents = 5+ (int) Math.random()*5+1;
    String[] students = new String[numStudents]; // declares array and allocates space
    System.out.print("Enter "+numStudents+" student names: ");
    for(int i=0; i<numStudents; i++){
      students[i] = myScanner.next();
    }
    // grades
    int[] midterm = new int[numStudents];
    for(int i=0; i<numStudents; i++){
      midterm[i] = (int) (Math.random()*100)+1;
    }
    // print
    for(int i=0; i<numStudents; i++){
      System.out.println(students[i]+" : "+midterm[i]);
    }
  }
}