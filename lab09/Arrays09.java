///////////////////////////////////////////////////////////////////////////////////////////////////////
/// Daswson Wallace 4/13/18 CSE2 Spring 2018
//
// Practices passing arrays inside methods
//
import java.util.Random;
public class Arrays09{
  public static int[] copy(int[] a){
    int[] copy = new int[a.length];
    for(int i=0; i<a.length; i++){
      copy[i]=a[i];
    }
    return copy;
  }
  public static void inverter(int[] a){
    for(int i=0; i<=a.length/2; i++){
      int temp = a[i];
      a[i] = a[a.length-1-i];
      a[a.length-1-i] = temp;
    }
  }
  public static int[] inverter2(int[] a){
    int[] copy = copy(a);
    for(int i=0; i<=copy.length/2; i++){
      int temp = copy[i];
      copy[i] = copy[copy.length-1-i];
      copy[copy.length-1-i] = temp;
    }
    return copy;
  }
  public static void print(int[] a){
    for(int i=0; i<a.length; i++){
      System.out.print(a[i]+" ");
    }
    System.out.println(" ");
  }
  public static void main(String[] args){
    Random random = new Random();
    int[] array0 = new int[10];
    for(int i=0; i<array0.length; i++){
      array0[i] = random.nextInt(10);
    }
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    print(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int[] array3 = inverter2(array2);
    print(array3);
  }
}