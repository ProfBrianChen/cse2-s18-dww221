///////////////////////////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 4/10/18 CSE2 Spring 2018
//
// Prompts the user to enter 15 ints for students" final grades in CSE2 in increasing order.
// Rejects non-integers, integers outside the range of 0-100, and integers that aren't greater than 
// ot equal to the previous.
//
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void linear(int[] grades, int grade){
    for(int i = 0; i<15; i++){
      if(grades[i]==grade){
        System.out.println(grade+" was found");
        return;
      }
    }
    System.out.println(grade+" was not found");
  }
  public static void binary(int[] grades, int grade){
    int start = 0;
    int stop = 15;
    int iterations = 0;
    while(start<stop){
      iterations++;
      int mid = ((stop-start)/2) + start;
      if(grades[mid]==grade){
        System.out.println(grade+" was found in the list with "+iterations+" iterations");
        return;
      }
      if(grade>grades[mid]){
        start = mid+1;
      }
      if(grade<grades[mid]){
        stop = mid-1;
      }
    }
    System.out.print(grade+" was not found in the list with "+iterations+" iterations");
  }
  public static void scramble(int[] grades){
    Random random = new Random();
    for(int i = 0; i<15; i++){
      int n = i + random.nextInt(15-i);
      int temp = grades[i];
      grades[i] = grades[n];
      grades[n] = temp;
    }
    for(int i=0; i<15; i++){
      System.out.print(grades[i]+" ");
    }
    System.out.println(" ");
  }
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int[] grades = new int[15];
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");
    for(int i = 0; i<15;){
      if(!myScanner.hasNextInt()){// rejects non-integrals
        System.out.println("Make sure it's an integer");
      }
      else{
        grades[i] = myScanner.nextInt();
        if(grades[i]<0 || grades[i]>100){// rejects imputs out of range
          System.out.println("Make sure your input is between 0 and 100");
        }
        else if(i==0){ // so there isn't an error whem i = 0
          i++;
        }
        else if(grades[i]<grades[i-1]){ // so each input is greater than the last
          System.out.println("Make sure each input is greater than or equal to the last");
        }
        else{
          i++;
        }
      }
    }
    // asks user for grade to search for
    System.out.print("Enter a grade to search for: ");
    int grade = myScanner.nextInt();
    binary(grades, grade); // binary search for grade
    System.out.println("Scrambled: ");
    scramble(grades); //scrables list
    System.out.print("Enter a grade to search for: ");
    grade = myScanner.nextInt();
    linear(grades, grade); // linear search for grades
  }
}