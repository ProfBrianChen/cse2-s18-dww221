/////////////////////////////////////////////////////////////////////////////////////////////
/// Dawosn Wallace 4/10/18 CSE2 Spring 2018
//
// Makes an aread of 10 random varaibles from 0 to 9. Has two methods, one that removes a 
// position in an array, and one that removes a value.
//
import java.util.Random;
import java.util.Scanner;
public class RemoveElements{
  // generates array with 10 random integers between 0 and 9
  public static int[] randomInput(){
    Random random = new Random();
    int[] list = new int[10];
    for(int i=1; i<10; i++){
      list[i] = random.nextInt(9);
    }
    return list;
  }
  // deletes postion in array
  public static int[] delete(int[] list, int pos){
    if(pos<0 || pos>9){
      System.out.println("The index is not valid");
      return list;
    }
    int[] listD = new int[list.length-1];
    for(int i=0; i<listD.length; i++){
      if(i>=pos){
        listD[i] = list[i+1];
      }
      else if(i<pos){
        listD[i] = list[i];
      }
    }
    return listD;
  }
  // deletes target variable in array
  public static int[] remove(int[] list, int target){
    int n = 0;
    int num = 0;
    for(int i=0; i<list.length; i++){
      if(list[i]==target){
        num++;
      }
    }
    int[] listR = new int[list.length-num];
    for(int i=0; i<listR.length; i++){
      while(list[n]==target){
        n++;
      }
      listR[i] = list[n];
      n++; 
    }
    return listR;
  }
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index = 0,target;
    String answer="";
    do{
        System.out.print("Random input 10 ints [0-9]");
        num = randomInput();
        String out = "The original array is:";
        out += listArray(num);
        System.out.println(out);

        System.out.print("Enter the index ");
        index = scan.nextInt();
        newArray1 = delete(num,index);
        String out1="The output array is ";
        out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
        System.out.println(out1);

        System.out.print("Enter the target value ");
        target = scan.nextInt();
        newArray2 = remove(num,target);
        String out2="The output array is ";
        out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);

        System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
        answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
}