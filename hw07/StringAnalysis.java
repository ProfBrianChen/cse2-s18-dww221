//////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/27/18 CSE2 Spring 2018
//
// Processes a string by examining all the characters, or just a specific number
// of characters in the string, and determines if they are letters. Lets the 
// user enter the string and choose the number of charcters they want examined.
//
import java.util.Scanner;
public class StringAnalysis{ 
  public static boolean string(String asdf){ // evaluates all chracters
    boolean letters = false;
    for(int i = 0; i < asdf.length(); i++){
      char x = asdf.charAt(i);
      if(Character.isLetter(x)){
        System.out.print("1");
        letters = true;
      }
      else{
        System.out.print("0");
      }
    }
    System.out.println(" ");
    return letters; 
  }
  public static boolean string(String asdf, int num){ //evaluates some characters
    boolean letters = false;
    for(int i = 0; i < num; i++){
      char x = asdf.charAt(i);
      if(Character.isLetter(x)){
        System.out.print("1");
        letters = true;
      }
      else{
        System.out.print("0");
      }
    }
    System.out.println(" ");
    return letters;
  }
  //main method
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    // prompts the user for string 
    System.out.print("Please input your string: ");
    String asdf = myScanner.next();
    int num = 0; // fixes variable scopng
    while(1==1){
      // asks user if they want to examine some or all characters
      System.out.print("Would you like to examine 'some' or 'all' characters: ");
      String amount = myScanner.next();
      if(amount.equals("all")){
        break;
      }
      else if(amount.equals("some")){ // prompts user for number they want to examine
        while(1==1){
          System.out.print("How namy characters do you want to examine: ");
          if(myScanner.hasNextInt()){
            num = myScanner.nextInt();
            break;
          }
          else{
            myScanner.next();
            System.out.println("Please input an integer"); //rejects wrong input
          }
        }
        break;
      }
      else{
        System.out.println("Please select 'some' or 'all'"); // rejects wrong input
      }
    }
    // evaluates string
    boolean letters = false; // true if there are letters in the string
    if(num==0 || num >= asdf.length()){ // if all characters are evaluated
      letters = StringAnalysis.string(asdf);
    }
    else{ // if some characters are evaluated
      letters = StringAnalysis.string(asdf, num);
    }
    System.out.println("1 means letter, 0 means non-letter"); // notifys user of output significance
    // reveals if there are letters in the string
    if(letters){
      System.out.println("There are letters in the string");
    }
    else{
      System.out.println("There are no letters in the string");
    }
  }
}