////////////////////////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/27/18 CSE2 Spring 2018
//
// Calculates the area of three different shapes; a rectangle, a triangle, and a circle. 
// Lets the user input the shape and the dimensions.
//
import java.util.Scanner;
public class Area{
  public static double rectangle(){ //calcualtes area for rectangle based on user input
    System.out.print("Please input the length of the rectangle: ");
    double length = Area.input();
    System.out.print("Please input the width of the rectangle: ");
    double width = Area.input();
    double area = length * width;
    return area;
  }
  public static double triangle(){ //calculates area for rectangle
    System.out.print("Please input the height of the triangle: ");
    double height = Area.input();
    System.out.print("Please input the length of the base of the triangle: ");
    double base = Area.input();
    double area = base * height / 2;
    return area;
  }
  public static double circle(){ //calculates area for circle
    System.out.print("Please input the radius of the circle: ");
    double radius = Area.input();
    double area = 3.1415 * radius * radius;
    return area;
  }
  public static double input(){ //takes user input and checks that it is a double
    Scanner myScanner = new Scanner(System.in);
    while(1==1){
      if(myScanner.hasNextDouble()){
        return myScanner.nextDouble();
      }
      else{
        System.out.print("Please input a double: ");
        myScanner.next();
      }
    }
  }
  public static void main(String[] args){ //main method required for every java program
    Scanner myScanner = new Scanner(System.in);
    double area = 0;
    while(1==1){
      //prompts user for shape 
      System.out.print("Please choose a shape (rectangle, triangle, circle): ");
      String shape = myScanner.next();
      if(shape.equals("rectangle")){
        area = Area.rectangle();
        break;
      }
      else if(shape.equals("triangle")){
        area = Area.triangle();
        break;
      }
      else if(shape.equals("circle")){
        area = Area.circle();
        break;
      }
      else{
        System.out.println("Make sure your input is all lower case");
      }
    }
    //prints output
    System.out.println("Area: "+area);
  }
}