//////////////////////////////////////////////////////////////////
/// Dawson Wallace 3/9/18 CSE2 Spring 2018
//
// Prints an encrypted x that varies in size based on user input.
//
import java.util.Scanner;
public class encrypted_x{
  public static void main(String[] args){
    //declares scanner
    Scanner myScanner = new Scanner(System.in);
    int input = 0;//initializes input
    //prompts size input and regects incorrect inputs
    System.out.print("Size integer bextween 0 and 100: ");
    while(1==1){
      if(myScanner.hasNextInt()){
      input = myScanner.nextInt();
        if(0<input && input<100){
          break;
        }
        else{
          System.out.print("Input an integer between 0 and 100: ");
        }
      }
      else{
        System.out.print("Input an integer between 0 and 100: ");
        myScanner.next();
      }
    }
    //prints x
    for(int i=0;i<input;i++){//prints lines
      for(int j=0;j<input;j++){//prints what's on the lines
        if(j==i || input-i==j+1){
          System.out.print(" ");
        }
        else{
          System.out.print("*");
        }
      }
      System.out.println(" ");
    }
  }
}